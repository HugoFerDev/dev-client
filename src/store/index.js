import { applyMiddleware, compose, createStore } from 'redux';

import logger from 'redux-logger';
import rootReducer from '../reducers';
import thunk from 'redux-thunk';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const middleware = applyMiddleware(thunk, logger);

const store = createStore(
  rootReducer,
  compose(
    middleware,
    composeEnhancers,
  ),
);

export default store;
