import { ADD_CLIENTE, FETCH_CLIENTE, REQUESTING } from '../actions';

const initialState = [];

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_CLIENTE: {
      const { cliente } = action;
      return [cliente].concat(state);
    }

    case FETCH_CLIENTE: {
      const { cliente } = action;
      return cliente;
    }

    default: {
      return state;
    }
  }
};
