import cliente from './cliente';
import { combineReducers } from 'redux';
import error from './error';

const rootReducer = combineReducers({
  cliente,
  error
});

export default rootReducer;
