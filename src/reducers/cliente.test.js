import cliente from './cliente';

describe('cliente reducer', () => {
  const state = [];

  test('Should return the initial state', () => {
    expect(cliente(undefined, [])).toEqual(state);
  });

  test('Should handle FETCH_CLIENTE', () => {
    const action = {
      type: 'FETCH_CLIENTE',
      cliente: { cliente: {} },
    };

    expect(cliente(undefined, action)).toEqual({ cliente: {} });
  });

  test('Should handle ADD_CLIENTE', () => {
    const action = {
      type: 'ADD_CLIENTE',
      cliente: { _id: '2151515' },
    };

    expect(cliente(undefined, action)).toEqual([{ _id: '2151515' }]);
  });
});
