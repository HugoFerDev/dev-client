import {
  ADD_CLIENTE,
  ADD_ERROR,
  FETCH_CLIENTE,
  UPDATE_CLIENTE,
} from '.';

import enviroment from '../enviroment';

export const addCliente = cliente => dispatch => new Promise((resolve, reject) => {
  fetch(`${enviroment.apiUrl}cliente`, {
    method: 'post',
    body: JSON.stringify(cliente),
    headers: { 'Content-Type': 'application/json' },
  })
    .then((response) => {
      if (response.status == 201) {
        return response.json();
      }
      throw response.json();
    })
    .then((data) => {
      dispatch({
        type: ADD_CLIENTE,
        cliente: data,
      });
      resolve(data);
    })
    .catch((err) => {
      err.then((error) => {
        dispatch({
          type: ADD_ERROR,
          error,
        });
      });
      reject(error);
    });
});

export const updateCliente = cliente => dispatch => new Promise((resolve, reject) => {
  fetch(`${enviroment.apiUrl}cliente`, {
    method: 'put',
    body: JSON.stringify(cliente),
    headers: { 'Content-Type': 'application/json' },
  })
    .then((response) => {
      if (response.status == 200) {
        return response.json();
      }
      throw response.json();
    })
    .then(() => {
      dispatch(fetchCliente());
      resolve();
    })
    .catch((err) => {
      err.then((error) => {
        dispatch({
          type: ADD_ERROR,
          error,
        });
      });
      reject(err);
    });
});

export const deleteCliente = id => dispatch => new Promise((resolve, reject) => {
  fetch(`${enviroment.apiUrl}cliente/${id}`, {
    method: 'delete',
    headers: { 'Content-Type': 'application/json' },
  })
    .then((response) => {
      if (response.status == 200) {
        resolve();
        return dispatch(fetchCliente());
      }
      throw response.json();
    })
    .catch((err) => {
      err.then((error) => {
        dispatch({
          type: ADD_ERROR,
          error,
        });
      });

      reject();
    });
});

export const fetchCliente = () => (dispatch) => {
  fetch(`${enviroment.apiUrl}cliente`, {
    method: 'get',
    headers: { 'Content-Type': 'application/json' },
  })
    .then((response) => {
      if (response.status == 200) {
        return response.json();
      }
      throw response.json();
    })
    .then(data => dispatch({
      type: FETCH_CLIENTE,
      cliente: data,
    }))
    .catch((err) => {});
};
