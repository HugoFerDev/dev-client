export const ADD_CLIENTE = 'ADD_CLIENTE';
export const FETCH_CLIENTE = 'FETCH_CLIENTE';
export const UPDATE_CLIENTE = 'UPDATE_CLIENTE';
export const ADD_ERROR = 'ADD_ERROR';
export const DELETE_ERROR = 'DELETE_ERROR';
