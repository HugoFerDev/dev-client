import * as actions from './cliente';

import configureMockStore from 'redux-mock-store';
import enviroment from '../enviroment';
import fetchMock from 'fetch-mock';
import thunk from 'redux-thunk';

// Redux

const mockStore = configureMockStore([thunk]);

const response = [
  {
    _id: '5ce0b16b34c9672dd87f5455',
    celular: '(51) 51584-8484',
    documento: '555.555.555-44',
    email: 'hugofernank@gmail.com',
    nome: 'Hugo',
    telefone: '(62) 98250-2664',
    tipo: 'fisica',
    criado: '2019-05-19T01:29:15.597Z',
    __v: 0,
  },
  {
    _id: '5ce0b19934c9672dd87f5456',
    celular: '(48) 44178-4747',
    documento: '51.584.181/8717-84',
    email: 'hugo_fernandes2010@hotmail.com',
    nome: 'Hugo Vieira',
    telefone: '(84) 84849-484',
    tipo: 'juridica',
    criado: '2019-05-19T01:30:01.346Z',
    __v: 0,
  },
];

describe('Actions / Action Creators', () => {
  let store;

  beforeEach(() => {
    store = mockStore();
  });

  afterEach(() => {
    fetchMock.restore();
  });

  // Parâmetros
  test('fetch_cliente / fetch_cliente', async () => {
    fetchMock.mock('*', response).catch(() => {});

    const expectedActions = [{ type: 'FETCH_CLIENTE' }];

    await store.dispatch(actions.fetchCliente());

    expect(store.getActions()).toEqual(expectedActions);
  });

  // test('get_parametros_request / get_parametros_error', async () => {
  //   fetchMock.mock(apollo_gateway_url, response.error);

  //   const expectedActions = [
  //     { type: 'GET_PARAMETROS_REQUEST' },
  //     { type: 'GET_PARAMETROS_ERROR' }
  //   ];

  //   await store.dispatch(actions.get_parametros());

  //   expect(store.getActions()).toEqual(expectedActions);
  // });

  // test('get_parametros_request / get_parametros_error', async () => {
  //   fetchMock.mock(apollo_gateway_url, 500);

  //   const expectedActions = [
  //     { type: 'GET_PARAMETROS_REQUEST' },
  //     { type: 'GET_PARAMETROS_ERROR' }
  //   ];

  //   await store.dispatch(actions.get_parametros());

  //   expect(store.getActions()).toEqual(expectedActions);
  // });

  //   // Classes
  //   test('get_classes_request / get_classes_success', async () => {
  //     fetchMock.mock(apollo_gateway_url, response.success.classes);

  //     const expectedActions = [
  //       { type: 'GET_CLASSES_REQUEST' },
  //       {
  //         type: 'GET_CLASSES_SUCCESS',
  //         response: [
  //           {
  //             id: 2006,
  //             descricao: 'Precatório'
  //           },
  //           {
  //             id: 2014,
  //             descricao: 'Requisição de Pequeno Valor'
  //           }
  //         ]
  //       }
  //     ];

  //     await store.dispatch(actions.get_classes());

  //     expect(store.getActions()).toEqual(expectedActions);
  //   });

  //   test('get_classes_request / get_classes_error', async () => {
  //     fetchMock.mock(apollo_gateway_url, response.error);

  //     const expectedActions = [
  //       { type: 'GET_CLASSES_REQUEST' },
  //       { type: 'GET_CLASSES_ERROR' }
  //     ];

  //     await store.dispatch(actions.get_classes());

  //     expect(store.getActions()).toEqual(expectedActions);
  //   });

  //   test('get_classes_request / get_classes_error', async () => {
  //     fetchMock.mock(apollo_gateway_url, 500);

  //     const expectedActions = [
  //       { type: 'GET_CLASSES_REQUEST' },
  //       { type: 'GET_CLASSES_ERROR' }
  //     ];

  //     await store.dispatch(actions.get_classes());

  //     expect(store.getActions()).toEqual(expectedActions);
  //   });

  //   test('set_tipo_peticao', () => {
  //     const expectedAction = {
  //       type: 'SET_TIPO_PETICAO',
  //       tipo_peticao: {}
  //     };

  //     expect(actions.set_tipo_peticao({})).toEqual(expectedAction);
  //   });

  //   test('set_categoria', () => {
  //     const expectedAction = {
  //       type: 'SET_CATEGORIA',
  //       categoria: {}
  //     };

  //     expect(actions.set_categoria({})).toEqual(expectedAction);
  //   });
});
