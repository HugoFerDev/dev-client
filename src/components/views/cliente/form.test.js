import { render } from 'react-testing-library';

import { Provider } from 'react-redux';
import React from 'react';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import Form from './form';

const mockStore = configureMockStore([thunk]);

const cliente = {
  _id: '6545sdfsd',
  celular: '555555555',
  documento: '5151515',
  email: 'dsfsd@fsdfsd.com',
  nome: 'sdfsd',
  telefone: 'sdfsd',
  tipo: 'fisica',
};

const store = mockStore({ cliente });

describe('Form', () => {
  let props;

  beforeEach(() => {
    props = {
      cliente,
      values: {},
      errors: {},
      touched: {},
      handleSubmit: jest.fn(),
      handleChange: jest.fn(),
      resetForm: jest.fn(),
      setFieldValue: jest.fn(),
      hideModal: jest.fn(),
    };
  });

  test('renders the component', () => {
    const { getByTestId } = render(
      <Provider {...{ store }}>
        <Form {...props} />
      </Provider>,
    );
    expect(getByTestId('form')).toBeInTheDocument();
  });
});
