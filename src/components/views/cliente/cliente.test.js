import { fireEvent, render } from 'react-testing-library';

import { Provider } from 'react-redux';
import React from 'react';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import Cliente from './index';

const mockStore = configureMockStore([thunk]);

const cliente = [
  {
    _id: '6545sdfsd',
    celular: '555555555',
    documento: '5151515',
    email: 'dsfsd@fsdfsd.com',
    nome: 'sdfsd',
    telefone: 'sdfsd',
    tipo: 'fisica',
  },
];

const store = mockStore({ cliente, error: {} });

describe('Form', () => {
  let props;

  beforeEach(() => {
    props = {
      cliente,
      addCliente: jest.fn(),
      updateCliente: jest.fn(),
      fetchCliente: jest.fn(),
      deleteCliente: jest.fn(),
    };
  });

  test('renders the component', () => {
    const { getByTestId } = render(
      <Provider {...{ store }}>
        <Cliente {...props} />
      </Provider>,
    );
    expect(getByTestId('table')).toBeInTheDocument();
  });

  test('open "form"', () => {
    const { getByTestId, getByText } = render(
      <Provider {...{ store }}>
        <Cliente {...props} />
      </Provider>,
    );

    fireEvent.click(getByText('Cadastrar'));

    expect(getByTestId('form')).toBeInTheDocument();
  });
});
