import {
  Button,
  Fieldset,
  InputMessageError,
  RadioButton,
  TextField,
} from '../../library';
import { Cnpj, Cpf, Telefone } from '../../../utils/mask';
import { Col, Row } from 'antd';

import { Formik } from 'formik';
import React from 'react';

const Form = props => (
  <div>
    <Formik
      enableReinitialize
      initialValues={{
        _id: props.cliente._id || '',
        celular: props.cliente.celular || '',
        documento: props.cliente.documento || '',
        email: props.cliente.email || '',
        nome: props.cliente.nome || '',
        telefone: props.cliente.telefone || '',
        tipo: props.cliente.tipo || 'fisica',
      }}
      validate={(values) => {
        const errors = {};
        if (!values.email) {
          errors.email = 'Campo Obrigatório';
        } else if (
          !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
        ) {
          errors.email = 'E-mail inválido';
        }

        if (!values.nome) {
          errors.nome = 'Campo Obrigatório';
        }

        if (!values.documento) {
          errors.documento = 'Campo Obrigatório';
        }
        return errors;
      }}
      onSubmit={(values) => {
        if (values._id) {
          props.updateCliente(values).then(() => {
            props.toastManager.add('Cliente atualizado com sucesso.', {
              appearance: 'success',
              autoDismiss: true,
              pauseOnHover: true,
            });
            props.hideModal();
          });
        } else {
          props.addCliente(values).then(() => {
            props.toastManager.add('Cliente cadastrado com sucesso.', {
              appearance: 'success',
              autoDismiss: true,
              pauseOnHover: true,
            });
            props.hideModal();
          });
        }
      }}
    >
      {({
        values,
        errors,
        touched,
        handleSubmit,
        handleChange,
        resetForm,
        setFieldValue,
      }) => (
        <form data-testid="form">
          <Fieldset legend="Cadastrar Cliente">
            <Row type="flex">
              <Col>
                <RadioButton
                  checked={values.tipo == 'fisica'}
                  title="Física"
                  name="tipo"
                  onChange={(e) => {
                    setFieldValue('documento', '');
                    setFieldValue('tipo', e.target.value);
                  }}
                  value="fisica"
                />
              </Col>
              <Col style={{ marginLeft: '1em' }}>
                <RadioButton
                  checked={values.tipo == 'juridica'}
                  title="Jurídica"
                  name="tipo"
                  onChange={(e) => {
                    setFieldValue('documento', '');
                    setFieldValue('tipo', e.target.value);
                  }}
                  value="juridica"
                />
              </Col>
            </Row>

            <Row style={{ marginTop: '1em' }}>
              <TextField
                title="Nome"
                name="nome"
                onChange={handleChange}
                value={values.nome}
                error={errors.nome && touched.nome && errors.nome}
              />
              <InputMessageError>
                {errors.nome && touched.nome && errors.nome}
              </InputMessageError>
            </Row>

            <Row>
              <TextField
                title={values.tipo == 'fisica' ? 'CPF' : 'CNPJ'}
                name="documento"
                mask="00.000.000-00"
                id="documento"
                maxLength={values.tipo == 'fisica' ? 14 : 18}
                value={values.documento}
                onChange={e => setFieldValue(
                  'documento',
                  values.tipo == 'fisica'
                    ? Cpf(e.target.value)
                    : Cnpj(e.target.value),
                )
                }
              />
              <InputMessageError>
                {errors.documento && touched.documento && errors.documento}
              </InputMessageError>
            </Row>

            <Row>
              <TextField
                title="E-mail"
                name="email"
                value={values.email}
                onChange={handleChange}
              />
              <InputMessageError>
                {errors.email && touched.email && errors.email}
              </InputMessageError>
            </Row>

            <Row>
              <Col span={7}>
                <TextField
                  title="Telefone"
                  name="telefone"
                  value={values.telefone}
                  maxLength="15"
                  onChange={e => setFieldValue('telefone', Telefone(e.target.value))
                  }
                />
              </Col>
              <Col style={{ marginLeft: '1em' }} span={7}>
                <TextField
                  title="Celular"
                  name="celular"
                  value={values.celular}
                  maxLength="15"
                  onChange={e => setFieldValue('celular', Telefone(e.target.value))
                  }
                />
              </Col>
            </Row>

            <Row type="flex" justify="end">
              <Button
                type="reset"
                onClick={() => {
                  props.hideModal();
                  resetForm();
                }}
              >
                Cancelar
              </Button>
              <Button
                primary
                type="button"
                onClick={() => handleSubmit(values)}
              >
                Salvar
              </Button>
            </Row>
          </Fieldset>
        </form>
      )}
    </Formik>
  </div>
);

export default Form;
