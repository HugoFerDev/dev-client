import {
  Divider, Icon, Modal, Row, Table, Tag,
} from 'antd';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  addCliente, deleteCliente, fetchCliente, updateCliente,
} from '../../../actions/cliente';

import { Button } from '../../library';
import Form from './form';
import Page from '../Page';

const { confirm } = Modal;

class Cliente extends Component {
  constructor(props) {
    super(props);
    this.state = { visible: false, cliente: {} };

    this.showModal = this.showModal.bind(this);
    this.hideModal = this.hideModal.bind(this);
    this.showDeleteConfirm = this.showDeleteConfirm.bind(this);

    document.title = 'Cliente';
  }

  componentDidMount() {
    this.props.fetchCliente();
  }

  showModal(cliente) {
    this.setState({
      visible: true,
      cliente: cliente || {},
    });
  }

  hideModal() {
    this.setState({
      visible: false,
      cliente: {},
    });
  }

  showDeleteConfirm(cliente) {
    const { deleteCliente, toastManager } = this.props;

    confirm({
      title: 'Voce tem certeza que deseja excluir esse cliente?',
      content: cliente.nome,
      okText: 'Sim',
      okType: 'danger',
      cancelText: 'Não',
      onOk() {
        deleteCliente(cliente._id).then(() => {
          toastManager.add('Cliente excluído com sucesso.', {
            appearance: 'success',
            autoDismiss: true,
            pauseOnHover: true,
          });
        });
      },
      onCancel() {},
    });
  }

  render() {
    const { addCliente, updateCliente, cliente } = this.props;
    const columns = [
      {
        title: 'Nome',
        dataIndex: 'nome',
        key: 'nome',
      },
      {
        title: 'Email',
        dataIndex: 'email',
        key: 'email',
      },
      {
        title: 'Celular',
        dataIndex: 'celular',
        key: 'celular',
      },
      {
        title: 'Tipo',
        key: 'tipo',
        dataIndex: 'tipo',
        render: tipo => (
          <span>
            {
              <Tag color={tipo == 'fisica' ? 'geekblue' : 'green'} key={tipo}>
                {tipo.toUpperCase()}
              </Tag>
            }
          </span>
        ),
      },
      {
        title: 'Documento',
        dataIndex: 'documento',
        key: 'documento',
      },
      {
        title: 'Ação',
        key: 'action',
        render: (text, record) => (
          <span>
            <Icon
              data-testid="open-form"
              type="edit"
              theme="outlined"
              onClick={() => this.showModal(record)}
              style={{ color: '#08c', fontSize: '20px', cursor: 'pointer' }}
            />
            <Divider type="vertical" />
            <Icon
              type="delete"
              theme="outlined"
              onClick={() => this.showDeleteConfirm(record)}
              style={{ color: '#FF0000', fontSize: '20px', cursor: 'pointer' }}
            />
          </span>
        ),
      },
    ];

    return (
      <Page>
        <Row type="flex" justify="end">
          <Button primary onClick={() => this.showModal()}>
            <Icon type="plus" />
            Cadastrar
          </Button>
        </Row>

        <Modal
          data-testid="modal"
          visible={this.state.visible}
          onCancel={e => this.hideModal(e)}
          okText="Salvar"
          width="40%"
          footer={[null, null]}
        >
          <Form
            data-testid="form"
            hideModal={() => this.hideModal()}
            addCliente={addCliente}
            updateCliente={updateCliente}
            cliente={this.state.cliente}
            toastManager={this.props.toastManager}
          />
        </Modal>

        <Table data-testid="table" columns={columns} dataSource={cliente} rowKey="nome" />
      </Page>
    );
  }
}

function mapStateToProps(state) {
  return {
    cliente: state.cliente,
    error: state.error,
  };
}

export default connect(
  mapStateToProps,
  {
    addCliente, updateCliente, fetchCliente, deleteCliente,
  },
)(Cliente);
